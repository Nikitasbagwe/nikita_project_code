package com.testing.avenuecode.basetest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.testing.avenuecode.base.Page;
import com.testing.avenuecode.pages.inbox.LandingPage;
import com.testing.avenuecode.pages.inbox.MySubTasksPage;
import com.testing.avenuecode.pages.inbox.MyTasksPage;
import com.testing.avenuecode.pages.login.LoginPage;

public class BaseTest {
	public static LandingPage landingpage=null;
	public static MyTasksPage taskPage =null;
	public static MySubTasksPage subtaskpage=null;
	
	
	@BeforeSuite
	public static MyTasksPage login(){
		
		/*if(Page.isLoggedIn==false){*/
			LoginPage loginpage = new LoginPage();
			landingpage=loginpage.doLogin();
			Page.isLoggedIn=true;
			taskPage = landingpage.gotoMyTasksPage();
/*			return taskPage;
		}*/
		return taskPage;
	}
	
	
	@AfterSuite
	public void close(){
		if(Page.driver!=null){
		Page.driver.quit();
		}
	}	
}
