package com.testing.avenuecode.testcases;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.testing.avenuecode.base.Page;
import com.testing.avenuecode.basetest.BaseTest;
import com.testing.util.TestUtil;

public class AddSubTaskAdd extends BaseTest {
	
	/*Testcase to Add SubTask and Verify SubTask Added*/
	@Test(dataProvider="getData", priority=1)
	public void AddSubTask(Hashtable<String,String> data){
		System.out.println("****Adding SubTask and Verifying SubTask Added****");
		subtaskpage = taskPage.GoToManageSubTasks();
		subtaskpage.callSubwindow();
		subtaskpage.AddSubTaskAdd(data.get("Description"), data.get("DueDate"));
		subtaskpage.scroll();
		subtaskpage.VerifySubTaskAdded(data.get("Description"));
		subtaskpage.CloseSubTask();
		}
	
	
	/*TestCase to verify the Manage Sub Tasks Button text with number of Sub tasks added by the user via. pop up*/
	@Test(priority=2)
	public void VerifyManageSubTasksButton(){
		System.out.println("****Verifying Manage Sub Tasks Button with number of Sub Tasks added****");
		String text = taskPage.SubTasksText("MANAGE_SUBTASKS");
		int number = Integer.parseInt(text);

		subtaskpage=taskPage.GoToManageSubTasks();
		subtaskpage.callSubwindow();
		int subtasksnumber = Page.driver.findElements(By.xpath("//tbody/tr[@ng-repeat='sub_task in task.sub_tasks']")).size();
		
		if(number==subtasksnumber){
			System.out.println("Correct Number of Sub Tasks displayed on Manage Tasks button!! ");
		}
		else{
			System.out.println("Number of Sub Tasks displayed on Manage Tasks button do not match with actual subtasks added by User!! ");
			Assert.fail("Number of Sub Tasks displayed on Manage Tasks button do not match with actual subtasks added by User!! ");
		}		
		subtaskpage.CloseSubTask();
	}
	
	

	@DataProvider
	public Object[][] getData(){
		return TestUtil.getData("AddSubTaskAdd", Page.xls1);
	}
}
