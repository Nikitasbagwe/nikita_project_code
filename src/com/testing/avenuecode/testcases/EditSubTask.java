package com.testing.avenuecode.testcases;

import org.testng.annotations.Test;
import com.testing.avenuecode.basetest.BaseTest;

public class EditSubTask extends BaseTest{
	
	/*Testcase to verify the subtasks edit*/
	@Test(priority=3)
	public void EditSubTasks(){
		System.out.println("****Editing SubTasks****");
		String text = taskPage.SubTasksText("MANAGE_SUBTASKS");
		int number = Integer.parseInt(text);
		if(number==0){
			System.out.println("No SubTasks in the first Task!!");
		}
		else{
		subtaskpage=taskPage.GoToManageSubTasks();
		subtaskpage.callSubwindow();
		subtaskpage.editSubTasks("Edited Sub Task via. Automation");
		subtaskpage.CloseSubTask();
		}
		}
	
}
