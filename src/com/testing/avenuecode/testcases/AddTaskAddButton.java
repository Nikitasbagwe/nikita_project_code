package com.testing.avenuecode.testcases;

import java.util.Hashtable;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.testing.avenuecode.base.Page;
import com.testing.avenuecode.basetest.BaseTest;
import com.testing.util.TestUtil;


public class AddTaskAddButton extends BaseTest {
	
	/*Testcase use to validate the welcome text*/
	@Test(priority=1)
	public void verifyPageText(){
		System.out.println("****Verify Welcome PageText****");
		String actual = taskPage.getPageText();
		String name = Page.CONFIG.getProperty("Name");
		String expected = "Hey "+ name+", this is your todo list for today:";
		
		if(actual==expected){
			System.out.println("Welcome Message matching!!");
			
		}
		else{
			System.out.println("Message Not Matching!! - Message should be " +expected);
			System.out.println();
			Assert.fail("Message Not Matching!! - Message should be " +expected);
		}
		
		}
	
	
	/*This testcase is use to Add the Task and verify if its added*/
	@Test(dataProvider="getData", priority=2)
	public void AddTask(Hashtable<String,String> data){
		System.out.println("****Adding Task and Verifying Task Added****");
		taskPage.AddTaskAddB(data.get("Description"));
		taskPage.VerifyTaskAdded(data.get("Description"));
		//Page.isLoggedIn=false;
		}


	
	@DataProvider
	public Object[][] getData(){
		return TestUtil.getData("AddTaskAddButton", Page.xls1);
	}

	
}
