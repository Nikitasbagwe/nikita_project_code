package com.testing.avenuecode.testcases;

import java.util.Hashtable;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.testing.avenuecode.base.Page;
import com.testing.avenuecode.basetest.BaseTest;
import com.testing.util.TestUtil;

public class AddTaskEnter extends BaseTest {
	
	/*This testcase is use to Add the Task via. Enter Key and verify if its added*/
	@Test(dataProvider="getData")
	public void CreateTaskEnter(Hashtable<String,String> data){
		
		System.out.println("****Adding Task via. Enter Key and Verifying Task Added****");
		Page.driver.navigate().refresh();
		taskPage.AddTaskEnt(data.get("Description"));
		taskPage.VerifyTaskAdded(data.get("Description"));

		}
	

	@DataProvider
	public Object[][] getData(){
		return TestUtil.getData("AddTaskEnter", Page.xls1);
	}
}
