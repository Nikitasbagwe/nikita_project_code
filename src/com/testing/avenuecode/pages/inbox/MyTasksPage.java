package com.testing.avenuecode.pages.inbox;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.testing.avenuecode.base.Page;

public class MyTasksPage extends Page{

	/*Function to get the My tasks Page text*/
	public String getPageText(){
		String text=driver.findElement(By.xpath(Page.OR.getProperty("welcomemsg"))).getText();
		return text;
	}
	
	/*Function to add Task via. Enter Key*/
	public void AddTaskEnt(String desc){
		input("task_desc",desc);
		enter("task_desc");
		System.out.println("Added the Task with Description--" +desc);
		if(desc==" "){
			System.out.println("Added the Task with Description--( ) i.e. space");
		} 
	}
	
	/*Function to Add Task via. Add(+) button*/
	public void AddTaskAddB(String desc){
		click("task_desc");
		input("task_desc",desc);
		click("ADDBUTTON");
				
		System.out.println("Added the Task with Description--" +desc);
		if(desc==" "){
			System.out.println("Added the Task with Description--( ) i.e. space");
		}
	}
	
	/*Function to verify the tasks which are added by Automation*/
	public boolean VerifyTaskAdded(String desc){
	List<WebElement> allTasks=	driver.findElements(By.xpath("//tbody/tr/td[2]"));
	Iterator<WebElement> itr = allTasks.iterator();
	
		while(itr.hasNext()){
		WebElement tmpEle=itr.next();
		String text = tmpEle.getText();
		
			if(text.equals(desc)){
			System.out.println("Task Found for Description---" +desc);
			
			if(getCountDesc(desc)<3){
				try{
					System.out.println("Task Created having description less than 2 characters!!!");
					System.out.println();
					takeScreenshot();
					Assert.fail("Task Created having description less than 2 characters!!!");}
				catch(Exception e){
					e.getMessage();
				}
				}
			else if(getCountDesc(desc)>250){
				try{
					System.out.println("Task Created having description more than 250 characters!!!");
					System.out.println();
					takeScreenshot();
					Assert.fail("Task Created having description more than 250 characters!!!");}
				catch(Exception e){
					e.getMessage();
				}
				}
			else if(getCountDesc(desc)>=3 && getCountDesc(desc)<=250){
				System.out.println("Task Created having description 3 to 250 characters!!!");
				System.out.println();
			}
			break;		
			}
			
			else if(text.equals("empty")||desc.equals(" ")){
				try{
					System.out.println("Task Found --'empty' for blank Description");
					System.out.println();
					takeScreenshot();
					Assert.fail("Task Found --'empty' for blank Description");
					}
				catch(Exception e){
					e.getMessage();
					}
			break;
			}
			}			
		return true;
		}

	/*Function to navigate to the Sub Tasks Pop up*/
	public MySubTasksPage GoToManageSubTasks(){
		WebDriverWait wait = new WebDriverWait(driver,10000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OR.getProperty("MANAGE_SUBTASKS")))).isDisplayed();
		click("MANAGE_SUBTASKS");
		return new MySubTasksPage();
	}
	
	/*Function to get the text of the Manage Sub Tasks button on My Tasks Page*/
	public String SubTasksText(String xpathKey){
		String textManage=	driver.findElement(By.xpath(OR.getProperty(xpathKey))).getText();
		String parts[] = textManage.split(" ");
		String part1 = parts[0];
		String num[] = part1.split("");
		String text1 = num[1];
		System.out.println(text1);
		return text1;
		}	
	
	
	
}
	
