package com.testing.avenuecode.pages.inbox;


import com.testing.avenuecode.base.Page;

public class LandingPage extends Page{

	public MyTasksPage gotoMyTasksPage(){
		
		click("mytaskslink");
	    return new MyTasksPage();	
	
	}
	
}
