package com.testing.avenuecode.pages.inbox;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.testing.avenuecode.base.Page;

public class MySubTasksPage extends Page {

/*Function to Add Sub Tasks*/ 
public void AddSubTaskAdd(String desc,String duedate){
		input("SUBTASK_DESC",desc);
		clear("DUEDATE");
		input("DUEDATE",duedate);
		click("SUBTASK_DESC");
		click("ADDSUBTASKBUTTON");
		
		if(desc.equals(" ")){
			System.out.println("Added the SubTask with Description--( ) i.e. space");
		}
		else if(duedate.equals(" ")){
			try{
			System.out.println("Added the SubTask with DueDate--( ) i.e. space and Description is--" +desc);
			System.out.println();
			takeScreenshot();
			CloseSubTask();
			Assert.fail("SubTask created with DueDate--( ) i.e. space");
			}
			catch(Exception e){
				System.out.println("Error here");
			}
		}
		else{
			System.out.println("Added the SubTask with Description-- " +desc);
		}
	}

/*Function to verify Sub Tasks which are added*/
	public boolean VerifySubTaskAdded(String desc){
		//scroll();
				List<WebElement> allTasks=	driver.findElements(By.xpath("//tbody/tr/td[2]"));
				Iterator<WebElement> itr = allTasks.iterator();
					while(itr.hasNext()){
					WebElement tmpEle=itr.next();
					String text = tmpEle.getText();
						
					if(text.equals(desc)){
						System.out.println("Sub Task Found for Description---" +desc);
						if(getCountDesc(desc)>250){
							try{
								System.out.println("Sub Task Created having description more than 250 characters!!!");
								System.out.println();
								CloseSubTask();
								Assert.fail("Sub Task Created having description more than 250 characters!!!");
								}
							catch(Exception e){
								e.getMessage();
							}//break;
							}
						else if(getCountDesc(desc)<=250){
							System.out.println("Sub Task Created having description upto 250 characters!!!");
							System.out.println();
						}
						break;
						
					}
					else if(text.equals("empty")){
						System.out.println("Sub Task Found --'empty' for blank Description");
						System.out.println();
						CloseSubTask();
						Assert.fail("Sub Task Found --'empty' for blank Description");
						//break;
					}
					
					}
					
					return true;
				}
				

	/*Function to Close the Sub Task*/
	public void CloseSubTask(){
		scroll();
		WebDriverWait wait = new WebDriverWait(driver, 5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Close')]")));
		click("CLOSE");
		}
	
	
	/*Function to switch to sub Tasks popup*/
	public void callSubwindow(){
		String subWindowHandler = null;
		Set<String> handles=driver.getWindowHandles();
		Iterator<String> iterator=handles.iterator();
		while(iterator.hasNext()){
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler);
		WebDriverWait wait = new WebDriverWait(driver,10000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id='new_sub_task']")));
		}
	
	/*Function to Edit Sub Tasks*/
	public void editSubTasks(String Editedtext){
		
		click("first_Subtask");
		clear("EDITABLE_SUBTASK");
		input(("EDITABLE_SUBTASK"),Editedtext);
		click("SUBMIT_BUTTON");
		scroll();
		click("CLOSE");
		click("MANAGE_SUBTASKS");
		
		WebDriverWait wait= new WebDriverWait(driver,5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody/tr[@ng-repeat='sub_task in task.sub_tasks']/td[2]/a")));
		
		List<WebElement> listele= driver.findElements(By.xpath("//tbody/tr[@ng-repeat='sub_task in task.sub_tasks']/td[2]/a"));
		Iterator<WebElement> itr = listele.iterator();
		
		while(itr.hasNext()){
			WebElement tmpEle=itr.next();
			String text1 = tmpEle.getText();

			if(text1.equals(Editedtext)){
					System.out.println("Sub Task Edited Successfully!!!");
					break;
			}
			else{
					System.out.println("Sub Task Not Edited");
					Assert.fail("Sub Task Not Edited");
			}
			}
	}
	
}
