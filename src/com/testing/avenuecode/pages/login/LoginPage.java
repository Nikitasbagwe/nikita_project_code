package com.testing.avenuecode.pages.login;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.firefox.FirefoxDriver;

import com.testing.avenuecode.base.Page;
import com.testing.avenuecode.pages.inbox.LandingPage;

public class LoginPage extends Page {
		
	
	public LandingPage doLogin(String UserName, String Password){
		System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\chromedriver\\geckodriver.exe");
		driver=new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get(CONFIG.getProperty("testSiteLandingPageURL"));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		input("EMAIL",UserName);
		input("PASSWORD",Password);
		click("SIGNIN_BUTTON");
		return new LandingPage();
	}
	
	/*Function to log into the Application*/
	public LandingPage doLogin(){
		return doLogin(CONFIG.getProperty("defaultUserName"),CONFIG.getProperty("defaultPassword"));
	}
}
