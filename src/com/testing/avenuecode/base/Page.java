package com.testing.avenuecode.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.testing.util.Xls_Reader;


public class Page {
	
	public static WebDriver driver=null;
	public static Properties CONFIG =null;
	public static Properties OR =null;
	public static Xls_Reader xls1= new Xls_Reader(System.getProperty("user.dir")+"//src//com//testing//xls//TestCases.xlsx");
	public static boolean isLoggedIn=false;


	public Page(){
		if(driver==null){
		// initialize the properties file
		CONFIG= new Properties();
		OR = new Properties();
		try{
			// config
			FileInputStream fs = new FileInputStream(System.getProperty("user.dir")+"\\src\\com\\testing\\config\\config.properties");
			CONFIG.load(fs);
			
			// OR
			fs = new FileInputStream(System.getProperty("user.dir")+"\\src\\com\\testing\\config\\OR.properties");
			OR.load(fs);
			}catch(Exception e){
			return;
		}


	}
		
}
	// click
	public void click(String xpathKey){
		try{
	        driver.findElement(By.xpath(OR.getProperty(xpathKey))).click();
		}catch(Exception e){
			System.out.println("error in click");
		}
	}
	
	//enter
	public void enter(String xpathKey){
		try{
	        driver.findElement(By.xpath(OR.getProperty(xpathKey))).sendKeys(Keys.ENTER);;
		}catch(Exception e){
			System.out.println("error in enter");
	}
	}
	
	
	// input
	public void input(String xpathKey, String text){
		try{
		driver.findElement(By.xpath(OR.getProperty(xpathKey))).sendKeys(text);
		}catch(Exception e){
			System.out.println("error in input");
		}
	}
	
	// clear
		public void clear(String xpathKey){
			try{
			driver.findElement(By.xpath(OR.getProperty(xpathKey))).clear();
			}catch(Exception e){
				System.out.println("error in clear");
			}
		}
		
	//take screen shot	
	public static void takeScreenshot() {

		Date d=new Date();
		String screenshotFile=d.toString().replace(":", "_").replace(" ", "_")+".png";
		String filePath=System.getProperty("user.dir")+"//Screenshots//"+screenshotFile;
		// store screenshot in that file
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		try {
			FileUtils.copyFile(scrFile, new File(filePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		}
	
	/*Get the count of description*/
	public int getCountDesc(String desc){
		char char_task[]=desc.toCharArray();
		int length_char_task = char_task.length;
		return length_char_task;
	}
	
	/*scroll through the Sub tasks page*/
	public void scroll(){
		
		WebElement element = driver.findElement(By.xpath("//button[contains(text(),'Close')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}


}
